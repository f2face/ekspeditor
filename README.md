<h1>Ekspeditor</h1>

Kontribusi
<ol>
<li>Fork project ini ke akun Gitlab anda.</li>
<li>Jalankan :
<pre>git clone https://gitlab.com/f2face/ekspeditor.git
git checkout -b contrib
</pre>

Kalo belum punya <strong>git</strong>, download di: <a href="https://git-scm.com/downloads" target="_blank">https://git-scm.com/downloads</a>
</li>
<li>Silakan edit dengan editor/IDE favorit anda.</li>
</ol>