<?php
/**
 * Ekspeditor: PHP Class Cek Resi & Tarif Jasa Ekspedisi
 * Jasa ekspedisi: JNE, TIKI, POS, SiCepat, Wahana, Atri Xpress, Pandu
 *
 * by Surya Oktafendri / @f2face <f2face@f2face.com>
 * 31/12/2015 - 8:42
 */

define('f2face.ekspeditor', true);

/** Kode ekspedisi :
 * JNE --> JNE
 * TIKI --> TIKI
 * POS --> POS
 * SiCepat --> sicepat
 * Wahana --> wahana
 * Atri Xpress --> atrix
 * Pandu --> pandu
 */
class Ekspeditor {
    protected $user_agent = 'Dalvik/1.6.0 (Linux; U; Android 4.2.2; Nexus S - 4.2.2 Build/JDQ39E)';

	/**
     * @param string $resi	: Nomor resi / consignment number / awb
     * @return object
     * @throws Exception
     */
    public function CekResi($resi) {
        $a = curl_init();
        curl_setopt($a, CURLOPT_URL, 'http://satubis.com/resta/index.v2.php');
        curl_setopt($a, CURLOPT_POST, true);
        curl_setopt($a, CURLOPT_HTTPHEADER, Array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($a, CURLOPT_POSTFIELDS, http_build_query(
            array(
                'v' => 2,
                'resi' => $resi
            )
        ));
        curl_setopt($a, CURLOPT_USERAGENT, $this->user_agent);
        curl_setopt($a, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($a, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($a, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($a, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($a, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($a, CURLOPT_TIMEOUT, 120);
        $out = curl_exec($a);
        if (!$out)
            throw new Exception(curl_error($a));
        curl_close($a);
        $obj = json_decode($out);
        if (!$obj)
            throw new Exception('Error parsing data');
		//return $obj;

        // Parsing data
        if (!empty($obj->error))
            throw new Exception($obj->error);
        list($_thn_kirim, $_bln_kirim, $_tgl_kirim) = explode('-', $obj->isi[2]);
        $out = array(
            'caption' => $obj->asal_tujuan,
            'no_resi' => $obj->isi[0],
            'ekspedisi' => $obj->eks,
            'layanan' => $obj->isi[1],
            'tgl_kirim' => $obj->isi[2],
            'status' => $obj->status,
            'asal' => $obj->isi[3],
            'tujuan' => $obj->isi[4],
            'pengirim' => $obj->isi[5],
            'penerima' => $obj->isi[6],
            'shipment_status' => $obj->isi[7],
            'resi_status' => $obj->isi[8],
            'status_terakhir' => $obj->isi[9],
            'lokasi_trace_terakhir' => $obj->isi[10]
        );
        $out['trace'] = array();
        $out['trace']['outbond'] = array();
        $_trc_outbond = array_reverse($obj->trc->Outbond);
        foreach ($_trc_outbond as $trc) {
            $_thn = ($trc[1] < $_bln_kirim) ? intval($_thn_kirim)+1 : $_thn_kirim;
            $_tgl = sprintf('%s-%s-%s', $_thn, $trc[1], $trc[0]);
            $out['trace']['outbond'][] = array(
                'tgl' => $_tgl,
                'waktu' => $trc[2],
                'lokasi' => $trc[3],
                'status' => $trc[4]
            );
        }
        $_dlv = $obj->trc->Delivery_time[0];
        $_dlv_thn = ($_dlv[1] < $_bln_kirim) ? intval($_thn_kirim)+1 : $_thn_kirim;
        $out['trace']['delivery'] = array(
            'tgl' => sprintf('%s-%s-%s', $_dlv_thn, $_dlv[1], $_dlv[0]),
            'waktu' => $_dlv[2],
            'lokasi' => $_dlv[3],
            'status' => $_dlv[4]
        );
        $_pod = $obj->trc->Pod_detail[0];
        $_pod_thn = ($_pod[1] < $_bln_kirim) ? intval($_thn_kirim)+1 : $_thn_kirim;
        $out['trace']['pod'] = array(
            'tgl' => sprintf('%s-%s-%s', $_pod_thn, $_pod[1], $_pod[0]),
            'waktu' => $_pod[2],
            'lokasi' => $_pod[3],
            'status' => $_pod[4]
        );
        return json_decode(json_encode($out));
    }

	/**
	 * @param string $ekspedisi	: Kode jasa ekspedisi
	 * @param string $asal		: Kode kota/wilayah asal
	 * @param string $tujuan	: Kode kota/wilayah tujuan pengiriman
	 * @param int $berat		: Berat paket/barang (kg)
	 * @return object
	 * @throws Exception
	 */
	public function CekTarif($ekspedisi, $kode_asal, $kode_tujuan, $berat = 1) {
		$a = curl_init();
        curl_setopt($a, CURLOPT_URL, 'http://satubis.com/resta/tarif/index.v2.php');
        curl_setopt($a, CURLOPT_POST, true);
        curl_setopt($a, CURLOPT_HTTPHEADER, Array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($a, CURLOPT_POSTFIELDS, http_build_query(
            array(
				'v' => 2,
				'kur' => $ekspedisi,
				'asl' => $kode_asal,
				'tuj' => $kode_tujuan,
				'brt' => $berat,
				'tp' => 'ha'
            )
        ));
        curl_setopt($a, CURLOPT_USERAGENT, $this->user_agent);
        curl_setopt($a, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($a, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($a, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($a, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($a, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($a, CURLOPT_TIMEOUT, 120);
        $out = curl_exec($a);
        if (!$out)
            throw new Exception(curl_error($a));
        curl_close($a);
        $obj = json_decode($out);
        if (!$obj)
            throw new Exception('Error parsing data');

        // Parsing data
        if (!empty($obj->error))
            throw new Exception($obj->error);
		$out = array(
			'ekspedisi' => $ekspedisi,
			'berat' => $berat,
			'layanan' => array()
		);
		if (empty($obj->tarif))
			throw new Exception('Data tidak ditemukan');
		foreach ($obj->tarif as $tarif) {
			$out['layanan'][] = array(
				'nama' => $tarif[0],
				'keterangan' => $tarif[1],
				'harga' => $tarif[2],
				'estimasi_waktu' => $tarif[3]
			);
		}
		return json_decode(json_encode($out));
	}

	/**
	 * @param string $ekspedisi	: Kode jasa ekspedisi
	 * @param string $keyword	: Kata kunci pencarian kota/wilayah asal
	 * @return object
	 * @throws Exception
	 */
	public function CariLokasiAsal($ekspedisi, $keyword) {
		if (strlen($keyword) < 3)
			throw new Exception('Keyword minimal 3 karakter');
		$a = curl_init();
		curl_setopt($a, CURLOPT_URL, 'http://satubis.com/resta/tarif/index.v2.php');
		curl_setopt($a, CURLOPT_POST, true);
		curl_setopt($a, CURLOPT_HTTPHEADER, Array(
			'Content-Type: application/x-www-form-urlencoded'
		));
		curl_setopt($a, CURLOPT_POSTFIELDS, http_build_query(
			array(
				'v' => 2,
				'kur' => $ekspedisi,
				'asl' => $keyword,
				'tp' => 'as'
			)
		));
		curl_setopt($a, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($a, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($a, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($a, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($a, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($a, CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($a, CURLOPT_TIMEOUT, 120);
		$out = curl_exec($a);
		if (!$out)
			throw new Exception(curl_error($a));
		curl_close($a);
		$obj = json_decode($out);
		if (!$obj)
			throw new Exception('Error parsing data');

		// Parsing data
		if (!empty($obj->error))
			throw new Exception($obj->error);
		if (empty($obj->results))
			throw new Exception('Hasil tidak ditemukan');
		$out = array(
			'ekspedisi' => $ekspedisi,
			'tipe' => 'asal',
			'hasil' => array()
		);
		foreach ($obj->results as $res) {
			$out['hasil'][] = array(
				'kode' => $res->city_code,
				'nama' => $res->city_name
			);
		}
		return json_decode(json_encode($out));
	}

	/**
	 * @param string $ekspedisi	: Kode jasa ekspedisi
	 * @param string $keyword	: Kata kunci pencarian kota/wilayah tujuan
	 * @return object
	 * @throws Exception
	 */
	public function CariLokasiTujuan($ekspedisi, $keyword) {
		if (strlen($keyword) < 3)
			throw new Exception('Keyword minimal 3 karakter');
		$a = curl_init();
		curl_setopt($a, CURLOPT_URL, 'http://satubis.com/resta/tarif/index.v2.php');
		curl_setopt($a, CURLOPT_POST, true);
		curl_setopt($a, CURLOPT_HTTPHEADER, Array(
			'Content-Type: application/x-www-form-urlencoded'
		));
		curl_setopt($a, CURLOPT_POSTFIELDS, http_build_query(
			array(
				'v' => 2,
				'kur' => $ekspedisi,
				'tuj' => $keyword,
				'tp' => 'tn'
			)
		));
		curl_setopt($a, CURLOPT_USERAGENT, $this->user_agent);
		curl_setopt($a, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($a, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($a, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($a, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($a, CURLOPT_CONNECTTIMEOUT, 60);
		curl_setopt($a, CURLOPT_TIMEOUT, 120);
		$out = curl_exec($a);
		if (!$out)
			throw new Exception(curl_error($a));
		curl_close($a);
		$obj = json_decode($out);
		if (!$obj)
			throw new Exception('Error parsing data');

		// Parsing data
		if (!empty($obj->error))
			throw new Exception($obj->error);
		if (empty($obj->results))
			throw new Exception('Hasil tidak ditemukan');
		$out = array(
			'ekspedisi' => $ekspedisi,
			'tipe' => 'tujuan',
			'hasil' => array()
		);
		foreach ($obj->results as $res) {
			$out['hasil'][] = array(
				'kode' => $res->city_code,
				'nama' => $res->city_name
			);
		}
		return json_decode(json_encode($out));
	}
}

// Test
$a = new Ekspeditor();
//var_dump($a->CekResi('MXGBE00094056015')); // JNE
//var_dump($a->CekResi('020169801905')); // TIKI
//var_dump($a->CekResi('8800002826848')); // Wahana
//var_dump($a->CekResi('EE147423716ID')); // EMS
//var_dump($a->CekResi('')); //
//var_dump($a->CariLokasiAsal('POS', 'jak'));
//var_dump($a->CariLokasiTujuan('POS', 'Tembilahan'));
//var_dump($a->CekTarif('JNE', 'Q0dLMTAwMDBK', 'UEtVMjA0MDBK'));
?>